//
//  LoginViewModel.swift
//  DesignPattern
//
//  Created by Nalinda Wickramarathna on 3/15/22.
//

import Foundation
import Combine

final class LoginViewModel {
    
    @Published var error: String?
    
    func login(email: String, password: String) {
        NetworkService.shared.login(email: email, password: password) { [weak self] success in
            self?.error = success ? nil : "Error Occured!"
        }
    }
}
