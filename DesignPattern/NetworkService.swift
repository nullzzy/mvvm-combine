//
//  NetworkService.swift
//  DesignPattern
//
//  Created by Nalinda Wickramarathna on 3/16/22.
//

import Foundation
import Combine

final class NetworkService {
    static let shared = NetworkService()
    
    private var user: User?
    
    private init() { }
    
    // Not used Combine
    func login(email: String, password: String, completion: @escaping(Bool) -> Void) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
            if email == "test@test.com" && password == "password" {
                self?.user = User(firstName: "Nalinda", lastName: "Wick",
                            email: "test@test.com", age: 30)
                completion(true)
            } else {
                self?.user = nil
                completion(false)
            }
        }
    }
    
    func getLoggedInUser() -> User {
        return user!
    }
    
    // Uses Combines Future
    func fetchCompanies() -> Future<[String], Error> {
        return Future { promise in
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                promise(.success(["Apple", "Microsoft", "Facebook", "Google"]))
            }
        }
    }
}
