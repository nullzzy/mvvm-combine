//
//  User.swift
//  DesignPattern
//
//  Created by Nalinda Wickramarathna on 3/15/22.
//

import Foundation

struct User {
    let firstName, lastName, email: String
    let age: Int
}
