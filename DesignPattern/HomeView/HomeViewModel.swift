//
//  HomeViewModel.swift
//  DesignPattern
//
//  Created by Nalinda Wickramarathna on 3/17/22.
//

import Foundation
import Combine

final class HomeViewModel {
    
    @Published var welcomeMessage: String?
    
    func getLoggedInUser() {
        let user = NetworkService.shared.getLoggedInUser()
        self.welcomeMessage = "Hello, \(user.firstName) \(user.lastName)"
    }
}
