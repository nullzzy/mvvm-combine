//
//  HomeViewController.swift
//  DesignPattern
//
//  Created by Nalinda Wickramarathna on 3/27/22.
//

import UIKit
import Combine

class MyCustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    let action = PassthroughSubject<String, Never>()
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        action.send("Cool button was tapped!")
    }
}

class HomeViewController: UIViewController {

    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel = HomeViewModel()
    private var cancellables: Set<AnyCancellable> = []
    
    private var companies: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBinders()
        viewModel.getLoggedInUser()
        
        // set table data
        // not the correct way of using viewmodel
        // but to test the combine features only
        setupTable()
    }
    
    private func setupBinders() {
        viewModel.$welcomeMessage.sink { [weak self] message in
            self?.welcomeLbl.text = message
        }.store(in: &cancellables)
    }
    
    private func setupTable() {
        NetworkService.shared.fetchCompanies()
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .finished:
                    print("Finished")
                case .failure(let error):
                    print("Error: \(error.localizedDescription)")
                }
        } receiveValue: { [weak self] value in
            self?.companies = value
            self?.tableView.reloadData()
        }.store(in: &cancellables)

    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.companies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyCustomTableViewCell
        cell.lblTitle.text = self.companies[indexPath.row]
        
        cell.action.sink { value in
            print("Value received: \(value)")
        }.store(in: &cancellables)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}
